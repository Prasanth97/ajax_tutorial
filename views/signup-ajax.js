


// CREATE/POST
$('#create-form').on('submit', function(event) {
    event.preventDefault();
    var createFirstName = $('#Firstname');
    var createLastName = $('#Lastname');
    var createEmail = $('#Email');
    var createPassword = $('#create-password');
    $.ajax({
        url: '/users',
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ first_name: createFirstName.val() ,last_name: createLastName.val(), email: createEmail.val(), password: createPassword.val() }),
        success: function(response) {
            console.log(response);
            createFirstName.val('');
            $('#get-button').click();
        }
    });
});

$(function() {
    // GET/READ
    $('#get-data').on('click', function() {
        $.ajax({
            url: '/get_users',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('tbody');

                tbodyEl.html('');

                response.products.forEach(function(product) {
                    var jsondata = product;
                    localStorage.setItem("id", jsondata._id);
                    console.log(jsondata.products, "11");                    // localStorage.setItem("id","product")
                    tbodyEl.append('\
                        <tr>\
                        <td><input type="text" class="id" value="' + product._id + '"></td>\
                        <td><input type="text" class="first_name" value="' + product.first_name + '"></td>\
                        <td><input type="text" class="last_name" value="' + product.last_name + '"></td>\
                        <td><input type="text" class="email" value="' + product.email + '"></td>\
                       <td>\
                                <button class="update-button">UPDATE/PUT</button>\
                                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" class="delete-button">DELETE</button>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });
})



$('table').on('click', '.update-button', function() {
    var rowEl = $(this).closest('tr');
   // localStorage.setItem("id", jsondata._id);

    var id = rowEl.find('.id').val();
     var new_first_name = rowEl.find('.first_name').val();
     var last_name = rowEl.find('.last_name').val();
     var email = rowEl.find('.email').val();


     console.log(new_first_name);


    $.ajax({
        url: '/update/'+id,
        method: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify({ new_first_name : new_first_name,last_name: last_name,email: email }),
        success: function(response) {
            console.log(response);
            $('#get-button').click();
        }
    });
});

$('table').on('click','.delete-button',function(){

    var rowEl = $(this).closest('tr');

    var id = rowEl.find('.id').val();
    console.log(id);

    $.ajax({
        url: '/delete/'+id,
        method: 'DELETE',
        contentType: 'application/json',
        data: JSON.stringify({ id : id }),
        success: function(response) {
            console.log(response);
            $('#get-button').click();
        }
    });
    window.location.href()


})
