var mongoose = require("mongoose");

var schemaStructure = mongoose.Schema({
    first_name: String,
    last_name: String,
    email: String,
    password: String,
  
});

var employeeData = mongoose.model("employeeDetails", schemaStructure);
module.exports = employeeData;