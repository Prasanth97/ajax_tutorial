var express = require('express');
 var bodyParser = require('body-parser');
 var mongoose = require('mongoose');
 var session = require('express-session');
 const users = require('./schema/emp_detail');
 mongoose.connect('mongodb://localhost:27017/emp_detail', {useNewUrlParser: true,useUnifiedTopology: true });
 const app = express();
 app.use(express.static('views'));
//  app.use(express.static(__dirname + "/public"));

 app.use(bodyParser.json());
 app.use(session({
    secret:'prasanth',
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 10000 }
}));
app.use(express.json())

app.post('/users', async(req,res)=>{
     const user = new users({first_name : req.body.first_name,last_name : req.body.last_name,email : req.body.email, password: req.body.password});
     let valid_user = await user.save();
     if(valid_user){
        res.status(200).send("User Detail Successfully Inserted");
    }else {
        res.status(500).send("Error Occured");
    }
})

app.get("/get_users", async(req, res) => {
    const valid_user = await users.find();
    if(valid_user){
        res.status(200).send({"products" : valid_user});
    } else {
        res.status(401).send("invalid user");
    }
});

app.post('/login', async (req,res) => {
    const email = req.body.email;
    const valid_user = await users.findOne({email: email});
    if(!valid_user || (valid_user.password != req.body.password)) {
        return res.status(401).send("password or email is incorrect ");
        } else if(valid_user && valid_user.password == req.body.password){
            return res.status(200).send("welcome to poorvika portal");
        }
     })

app.put('/update/:id',async (req,res) =>{
 const id = req.params.id;
 console.log(id);
await users.findByIdAndUpdate({_id:id},{first_name : req.body.new_first_name,last_name : req.body.last_name,email : req.body.email, password : req.body.password},function(err,docs){
    if(err || !docs){ 
        return res.status(404).send("invalid id");
    }
    else if(docs){
        return res.status(200).send(docs);
    }
    });
})

app.delete('/delete/:id',async (req,res)=>{
    console.log(req.params.id);
    await users.findByIdAndDelete({_id : req.params.id},(err,docs)=>{
         if(err){ 
           return res.send(err);
        }
        else if(docs){
           return res.send(docs);
         }
        })
    })
app.delete('/deleteAll',async(req,res)=>{
    await users.deleteMany({},(err,docs)=>{

        res.send(docs);

    })
})
 
// app.get('/home',async(req,res)=>{
//     //res.render("home.html")
// })

app.listen(3005);
    console.log("port connected on 3005");

